<!-- Begin Block 1 -->
	<section class="block_1" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns" data-equalizer data-equalize-on="medium" id="test-eq">
				<?php dynamic_sidebar( 'block_1' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 1 -->